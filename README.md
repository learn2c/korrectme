# Korrect Me

This is a Hackathon Project made for Bitcamp 2016, built by Teddy Li, Sean Appleby and Nathan Wolfe.

Korrect Me is a lightweight webapp that allows people to musicalize their vocalizations. 
>1. The user hums or sings a song into the recorder.
>2. The backend "midi-izes" the input and pushes out an output. The file recognizes different pitches from the original song.
>3. The backend brings out a midi file that is then played on the website, allowing for a switch to any instrument available.
